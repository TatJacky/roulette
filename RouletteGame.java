package Lab2.roulette;
import java.util.Scanner;

public class RouletteGame {
    public static void main(String[] args){
        
        int bank = 2000;
        Scanner user = new Scanner(System.in);
        int replay = 0;

        while(replay == 0){
            RouletteWheel myWheel = new RouletteWheel();
            boolean winCondition = false;

            System.out.println("Please enter the amount you would like to bet: ");
            int betAmount = user.nextInt();
            while( betAmount < 0 || betAmount > bank){
                System.out.println("Please enter a valid amount: ");
                betAmount = user.nextInt();
            }
            
            System.out.println("Please place your bets on the following: ");
            System.out.println("[0]Even, [1]Odd, [2]Specific Number, [3]Red(WIP), [4]Black(WIP)");
            int bet = user.nextInt();
            while(bet < 0 || bet > 2){
                bet = user.nextInt();
            }

            int betSpecific = 0;
            if(bet == 2){
                System.out.println("Please enter the specific value between 0 and 36: ");
                betSpecific = user.nextInt();
                while(betSpecific < 0 || betSpecific > 36){
                    System.out.println("Please enter a value between 0 and 36");
                    betSpecific = user.nextInt();
                }
            }

            System.out.println("Bets are in place!!");
            System.out.println("Spinning the Wheel");
            myWheel.spin();
            System.out.println("Roulette: "+ myWheel.getValue());

            if(bet == 0){
                if(myWheel.isEven() == true){
                    winCondition = true;
                }
            }

            if(bet == 1){
                if(myWheel.isOdd() == true){
                    winCondition = true;
                }
            }

            if(bet == 2){
                if(myWheel.getValue() == betSpecific){
                    winCondition = true;
                }
            }

            if(winCondition == true){
                System.out.println("Congratulations!! YOU WON: "+ betAmount);
                bank = bank + betAmount;
            }
            else {
                System.out.println("Better luck next time !!");
                bank = bank - betAmount;
            }
            System.out.println("You now have: "+bank);
            System.out.println("Would you like to play again ?");
            System.out.println("[0]Yes, [1]No");
            replay = user.nextInt();
            while(replay < 0 || replay > 1){
                System.out.println("[0]Yes, [1]No");
                replay = user.nextInt();
            }
        }
    }
}
