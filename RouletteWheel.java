package Lab2.roulette;
import java.util.Random;

public class RouletteWheel {
    private Random rand;
    private int lastSpin;

    //Constructor
    public RouletteWheel(){
        this.rand = new Random();
        this.lastSpin = 0;
    }
    
    //Methods
    public void spin(){
        this.lastSpin = rand.nextInt(37);
    }

    public int getValue(){
        return this.lastSpin;
    }

    public boolean isEven(){
        if(this.lastSpin % 2 == 0){
            return true;
        }
        return false;
    }

    public boolean isOdd(){
        if(this.lastSpin % 2 == 0){
            return false;
        }
        return true;
    }

}
